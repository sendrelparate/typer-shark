package DooDoo;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import javafx.geometry.Rectangle2D;
import javafx.scene.Group;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.text.Font;
import javafx.scene.text.Text;


public class Sprite {
	protected Image img;
	protected double dx,dy;
	protected double x,y;
	protected boolean visible;
	protected double width, height;


	public Sprite(double xPos, double yPos){
		this.x = xPos;
		this.y = yPos;
		this.visible = true;
	}

	protected void loadImage(Image img){
		try{
			this.img = img;
			this.setSize();
		}catch(Exception e){}
	}

	void render(GraphicsContext gc){
		gc.drawImage(this.img,this.x,this.y);
	}

	void renderDead(GraphicsContext gc){
		gc.drawImage(this.img,this.x,this.y+20);
	}

	void renderText(Shark shark, GraphicsContext gc, Group root) {
		String word = shark.getWord();
		gc.fillText(word,this.x+25,this.y+125);

	}//x+150 y+85nasa tail

	void renderBubble(GraphicsContext gc, Bubbles bubble){
		String word = bubble.getWord();
		gc.fillText(word, this.x, this.y);
	}

	void renderBubbleText(Bubbles bubble, GraphicsContext gc, Group root){
		String word = bubble.getWord();
		gc.fillText(word,this.x+50,this.y+80);
	}



	void renderPointsEarned(GraphicsContext gc, Shark shark) {
		double points;
		points = shark.getX() +shark.getPoints();
		String str = String.valueOf(points);
		gc.fillText(str, 550, 100);
	//	System.out.println(str);
	}
	
	void removeRenderTopPanel(TopPanel panel){
		for(Text tx: panel.texts){
			panel.root2.getChildren().remove(tx);
		}
		
		for(int i=0; i<panel.texts.size(); i++) {
			panel.texts.remove(i);
		}
	}
	
	void renderInput(GraphicsContext gc, Diver player, TopPanel panel) {
		Text input = new Text("Input: " + GameStage.currentWord);
		try {
			input.setFont(Font.loadFont(new FileInputStream("src/images/Kenney_Future.ttf"), 16));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		input.setLayoutX(450);
		input.setLayoutY(25);
		panel.texts.add(input);
		panel.root2.getChildren().add(input);
	}
	
	void renderPlayerLivesAndDepthAndZapper(GraphicsContext gc, Diver player, TopPanel panel) {
		Text scoreVal = new Text("Score: " + String.valueOf(player.getplayerPoints()));
		try {
			scoreVal.setFont(Font.loadFont(new FileInputStream("src/images/Kenney_Future.ttf"), 16));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		scoreVal.setLayoutX(650);
		scoreVal.setLayoutY(25);
		panel.texts.add(scoreVal);
		panel.root2.getChildren().add(scoreVal);
		
		/*
		Text input = new Text("Input: " + GameStage.currentWord);
		try {
			input.setFont(Font.loadFont(new FileInputStream("src/images/Kenney_Future.ttf"), 16));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		input.setLayoutX(450);
		input.setLayoutY(25);
		panel.texts.add(input);
		panel.root2.getChildren().add(input);
		*/
		Text lives = new Text("Lives: " + +player.getLives());
		try {
			lives.setFont(Font.loadFont(new FileInputStream("src/images/Kenney_Future.ttf"), 16));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		lives.setLayoutX(50);
		lives.setLayoutY(25);
		panel.texts.add(lives);
		panel.root2.getChildren().add(lives);

		float myFloat = player.getDepth();

		String formattedString = String.format("%.02f", myFloat);
		
		
		Text depth = new Text("Depth: " + formattedString);
		try {
			depth.setFont(Font.loadFont(new FileInputStream("src/images/Kenney_Future.ttf"), 16));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		depth.setLayoutX(250);
		depth.setLayoutY(25);
		panel.texts.add(depth);
		panel.root2.getChildren().add(depth);

		Text zapper = new Text("Zapper - ");
		try {
			zapper.setFont(Font.loadFont(new FileInputStream("src/images/Kenney_Future.ttf"), 16));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		zapper.setLayoutX(850);
		zapper.setLayoutY(25);
		panel.texts.add(zapper);
		panel.root2.getChildren().add(zapper);
		
		int percent = (int)(player.getEnergy() * 100)/1;
		Text progress = new Text(percent + "%");
		try {
			progress.setFont(Font.loadFont(new FileInputStream("src/images/Kenney_Future.ttf"), 16));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		progress.setLayoutX(1055);
		progress.setLayoutY(25);
		panel.texts.add(progress);
		panel.root2.getChildren().add(progress);

	}



	private void setSize(){
		this.width = this.img.getWidth();
		this.height = this.img.getHeight();
	}



	public double getX(){
		return this.x;
	}
	public double getY(){
		return this.y;
	}

	public boolean getVisible(){
		return visible;
	}
	public boolean isVisible(){
		if(visible){
			return true;
		}
		return false;
	}

	//method that will check for collision of two sprites
	public boolean collidesWith(Sprite rect2)	{
		Rectangle2D rectangle1 = this.getBounds();
		Rectangle2D rectangle2 = rect2.getBounds();

		return rectangle1.intersects(rectangle2);
	}
	//method that will return the bounds of an image
	private Rectangle2D getBounds(){
		return new Rectangle2D(this.x, this.y, this.width, this.height);
	}

// SETTERS
	public void setDX(int dx){
		this.dx = dx;
	}
	public void setDY(int dy){
		this.dy = dy;
	}
	public void setWidth(double val){
		this.width = val;
	}
	public void setHeight(double val){
		this.height = val;
	}
	public void setVisible(boolean val){
		this.visible = val;
	}
	public void incYPos(int depth) {
		this.y += depth;
	}
	public void incXPos(int val) {
		this.x += val;
	}

	public void moveLeft(double speed) {
		this.x -= speed;
	}

	public void moveUp(double speed) {
		this.y -= speed;
	}

	public void deadMoveDown(double speed) {
		this.y+= speed;
	}

	public void setX(){
		this.x -= 0.5000;
	}
}