package DooDoo;

import javafx.scene.image.Image;

public class Bubbles extends Sprite {
	public final static int BUBBLES_WIDTH = 150;
	public final static int BUBBLES_HEIGHT = 150;
	public final static Image BUBBLES_IMAGE = new Image("images/bubble.png",BUBBLES_WIDTH,BUBBLES_HEIGHT,false,false);
	protected String word;
	private boolean alive;
	private double speed;


	public Bubbles(int x, int y, String word) {
		super(x,y);
		this.alive = true;
		this.loadImage(BUBBLES_IMAGE);
		this.word = word;
		this.speed = 0.75;
	}

	public boolean isAlive(){
		return this.alive;
	}


	public void move() {
		this.moveUp(this.speed);
	}

	public boolean setBubbleDead(){
		this.alive = false;
		return this.alive;
	}

	public String getWord() {
		return this.word;
	}
}
