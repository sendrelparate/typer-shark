package DooDoo;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javafx.animation.AnimationTimer;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.text.Text;

import java.util.Random;

import java.io.*;



public class GameTimer extends AnimationTimer{
	private Group root;
	private GraphicsContext gc;
	private Scene theScene;
	private Diver diver;
	private ArrayList<Shark> sharks;
	private Map<Shark, Double> deadSharks;
	private ArrayList<Bubbles> bubbles;
	public static final int MAX_NUM_SHARKS = 5;
	public static final String WORD_FILE = "words.txt";
	private HashMap<String, Integer> listOfWords;
	private ArrayList<String> listOfKeys;
	Random r = new Random();
	private Shark currentShark;
	private Bubbles currentBubble;
	private boolean valid;
	private boolean bubblePop;
	private int timer;
	private TopPanel topPanel;


	GameTimer(GraphicsContext gc, Scene theScene, Group root){
		this.root = root;
		this.gc = gc;
		this.theScene = theScene;
		this.diver = new Diver("Player",100,0 - Diver.DIVER_HEIGHT);
		this.sharks = new ArrayList<Shark>();
		this.deadSharks = new HashMap<Shark, Double>();
		this.bubbles = new ArrayList<Bubbles>();

		this.spawnSharks();
		this.handleKeyPressEvent();


		this.valid = false;
		this.bubblePop = false;
		this.timer = 0;
		topPanel = new TopPanel();
		root.getChildren().add(topPanel);

	}





	@Override
	public void handle(long currentNanoTime){
		this.removeRenderTopPanel();
		this.gc.clearRect(0,0,GameStage.WINDOW_WIDTH,GameStage.WINDOW_HEIGHT);
		this.diver.move();
		
		if(bubblePop == true){
			this.timer++;
        	this.moveBubbleSharks();
        	//this.moveSharks();

			if(timer > 200){
				bubblePop = false;
				
	        	timer = 0;
			}




		}else {

			this.moveSharks();
		}


		this.diver.render(this.gc);
		this.renderSharks();
		//this.checkCollisions();
		if(this.diver.getLives()>0) {
			this.spawnAndRenderBubble();
		}

		this.moveBubbles();
		this.sharkDead();
		


		this.sharkDiverCollides();

		//this.loadWords(GameTimer.WORD_FILE);

	//	this.diver.renderPointsText(gc, this.diver, this.topPanel);
		this.diver.renderInput(gc, this.diver, this.topPanel);
		this.topPanel.setProgressBar(this.diver.getEnergy());
		this.diver.renderPlayerLivesAndDepthAndZapper(gc, this.diver, this.topPanel);
		



		if(this.diver.getDepth() != Diver.TOTAL_DEPTH && this.diver.isAlive() == true){
		      if(this.sharks.size() ==0){
		        this.spawnSharks();
		  //    }else if(this.sharks.size()==1) {
		    //	  this.spawnSharks();
		      }
		      if(this.diver.getDepth() >= 40) {
		    	  this.increaseSpeed(1.5);
		    	  if(this.sharks.size()==2) {
		    		  this.spawnSharks();
		    	  }
		      }
		      if(this.diver.getDepth() >= 60) {
		    	  this.increaseSpeed(2.0);
		    	  if(this.sharks.size()==2)
		    		  this.spawnSharks();
		    	
		      }
		}else {
			this.sharks.clear();
		}

		this.diver.incDepth();

		this.diver.setZapperAvailable();
		this.diver.incEnergy();
		

	}

	private void sharkDiverCollides() {
		/*
		for(Shark sh: this.sharks) {
			if(sh.getX()==this.diver.getX()+Diver.DIVER_WIDTH) {
				sh.changeImg(Shark.EXPLOSION);
				System.out.println("BAWAS BUHAY");
				this.diver.decLives();
			}else if(sh.getX()<this.diver.getX()-10) {
				sh.isDead();
				//this.sharks.remove(sh);
			}

		}
		*/
		for(Shark sh: this.sharks) {
			
			if(sh.getX() <= 250 && sh.getSharkExplosion() == false) {
				System.out.println(sh.getX());
				System.out.println("--");
				sh.changeImg(Shark.EXPLOSION);
				sh.setSharkExplosion();
				sh.setWord("");
				this.diver.decLives();
			}else if(sh.getX() < 100) {
				sh.isDead();
			}
		}

	}

	private void sharkDead(){
		Iterator<Map.Entry<Shark, Double>> entries = deadSharks.entrySet().iterator();
		while (entries.hasNext()) {
		    Map.Entry<Shark, Double> pair = entries.next();
		    if(pair.getKey().getY() < pair.getValue() +15){
		    	pair.getKey();
				pair.getKey().changeImg(Shark.SHARK_DEAD2);
		    	pair.getKey().deadMoveDown(0.5);
		    }else {
		    	entries.remove();
		    }
		}
		
		renderDeadSharks();






	}


	private void increaseSpeed(double val) {
		for(Shark sh: this.sharks) {
			sh.setSpeed(val);
		}
	}

	private void renderSharks(){
		for(Shark sh: this.sharks){
			sh.render(this.gc);
			sh.renderText(sh,this.gc, this.root);
		}
	}


	private void renderDeadSharks(){
		for (Map.Entry<Shark, Double> pair : deadSharks.entrySet()) {
			pair.getKey().renderDead(this.gc);
		}
	}


	private void spawnSharks(){

		this.generateWords(GameTimer.WORD_FILE);

		for(int i=0;i<GameTimer.MAX_NUM_SHARKS;i++){
			int x = GameStage.WINDOW_WIDTH-Shark.SHARK_WIDTH;

			String randomWord = listOfKeys.get(r.nextInt(listOfKeys.size()));
			int points = listOfWords.get(randomWord);
			
			Shark shark = new Shark(x+r.nextInt(200),GameStage.WINDOW_HEIGHT - (125 * (i + 2)),randomWord,points);
			
				this.sharks.add(shark);
			

		}
	}

	public void spawnAndRenderBubble() {
		int x = 0, y;
		this.generateWords(GameTimer.WORD_FILE);
		String randomWord = listOfKeys.get(r.nextInt(listOfKeys.size()));
		int i = r.nextInt(600);

		y = 800-Bubbles.BUBBLES_HEIGHT;
		while(x < 200){
			x = r.nextInt(1150);
		}

		if(i == 8){
			Bubbles bubble = new Bubbles(x,y, randomWord);
			this.bubbles.add(bubble);
		}
		
		renderBubbles();

	}

	public void renderBubbles() {
		/*
		for(Bubbles bb: this.bubbles){
			if(bb.isAlive()) {
				bb.render(this.gc);
				bb.renderBubbleText(bb,this.gc, this.root);
			}else {
				bubbles.remove(bb);
			}

		}
		*/
		for (Iterator<Bubbles> it = bubbles.iterator(); it.hasNext(); ) {

		    Bubbles bubble = it.next();
		    
			if(bubble.isAlive()) {
				bubble.render(this.gc);
				bubble.renderBubbleText(bubble,this.gc, this.root);
			}else {
				it.remove();
			}
			

		}
	}

	private void moveSharks(){
		for(int i=0;i<this.sharks.size();i++){
			Shark sh = this.sharks.get(i);
			if(sh.isAlive()){
				sh.move();
			}else{
				this.sharks.remove(sh);

			}
		}
	}
	
	private void moveBubbleSharks() {
		for(int i=0;i<this.sharks.size();i++){
			Shark sh = this.sharks.get(i);
			if(sh.isAlive()){
				sh.moveLeft(0.3);
			}else{
				this.sharks.remove(sh);

			}
		}
	}

	private void moveBubbles(){
		for(int i=0;i<this.bubbles.size();i++){
			Bubbles bubble = this.bubbles.get(i);
			if(bubble.getY()<0) {

				bubble.setBubbleDead();
				this.bubbles.remove(bubble);
			}
			
			if(bubble.isAlive()){
				bubble.move();
			} else {
				bubble.setBubbleDead();
				this.bubbles.remove(bubble);
			}
		}
	}

	private void handleKeyPressEvent(){
		this.theScene.setOnKeyPressed(new EventHandler<KeyEvent>(){
			public void handle(KeyEvent e){
				KeyCode code = e.getCode();
				String input = code.toString().toUpperCase();
				validateInput(input);

				if(e.getCode() == KeyCode.SPACE) {
					if (diver.isZapperAvailable()) {
						valid = false;
						GameStage.currentWord = "";
						for(Shark sh: sharks){
							deadSharks.put(sh, sh.getY());
							sh.isDead();
							diver.resetEnergy();
							play_zapper_audio();
						}
					}else {
						System.out.println("ZAPPER NOT AVAILABLE");
					}
				}
				if(e.getCode() == KeyCode.ESCAPE) { //To reset input buffer
					GameStage.currentWord="";
					currentShark = null;
					currentBubble = null;
					valid = false;
				}

			}
		});

	}
	
	/*
	private void checkCollisions(){
		for(Shark shark:this.sharks){
			if(shark.collidesWith(this.diver)){
				this.diver.decLives();
				shark.isDead();
				System.out.println("You were bitten by the shark!");
			}
		}
	}
	*/


	private void generateWords(String fileName) {
		this.listOfWords = new HashMap<String,Integer>();
		try {
			String[] arrOfStr;
			String str;
			BufferedReader rdr = new BufferedReader(new FileReader(fileName));

			while((str = rdr.readLine()) != null) {
				arrOfStr = str.split(" ");
				listOfWords.put(arrOfStr[0],Integer.parseInt(arrOfStr[1]));

			}
			this.listOfKeys = new ArrayList<String>(listOfWords.keySet());
			rdr.close();


		}catch(Exception e){
				e.getMessage();
				//e.printStackTrace();
			}

		}

	public void validateInput(String key) {


		if(this.valid == false){
			for(int i=0; i < this.bubbles.size(); i++){
				for(Bubbles bubble: this.bubbles){
					if(bubble.getWord().startsWith(GameStage.currentWord + key)){ // if there is a word that matches the current word correctly typed by the user
				          GameStage.currentWord += key; // accept the key press
				          currentBubble = bubble;
				          currentShark = null;
				          this.valid = true;
				          System.out.println(this.valid);
				          System.out.println(GameStage.currentWord);
				          break;

					}
				}
			}
			for(int i=0; i <GameTimer.MAX_NUM_SHARKS; i++) { //gets the words of the sharks
				for(Shark shark: this.sharks) {
					if(shark.getWord().startsWith(GameStage.currentWord + key)){ // if there is a word that matches the current word correctly typed by the user
				          GameStage.currentWord += key; // accept the key press
				          shark.incXPos(10);
				          currentShark = shark;
				          currentBubble = null;
				          this.valid = true;
				          System.out.println(this.valid);
				          System.out.println(GameStage.currentWord);
				          break;

					}


				}

			}
		}else if(currentBubble == null && valid == true){
			if(currentShark.getWord().startsWith(GameStage.currentWord + key)){ // if there is a word that matches the current word correctly typed by the user
					  GameStage.currentWord += key; // accept the key press
			          currentShark.incXPos(10);
			          System.out.println(GameStage.currentWord);
			          if(currentShark.getWord().equals(GameStage.currentWord)){ // if already equals
			        	  diver.incPoints(currentShark);
			        	  deadSharks.put(currentShark, currentShark.getY());
			              currentShark.isDead(); // shark will die
			              GameStage.currentWord = "";
			              this.valid = false;
			              play_death_audio();
			          }
		        }
				

		}else if(currentShark == null && valid == true){
			if(currentBubble.getWord().startsWith(GameStage.currentWord + key)){ // if there is a word that matches the current word correctly typed by the user
		        if(currentBubble.getY() < 0) {
		        	GameStage.currentWord = "";
		        	currentShark = null;
					currentBubble = null;
					valid = false;
		        }else {

					GameStage.currentWord += key; // accept the key press
	
			          System.out.println(GameStage.currentWord);
			          if(currentBubble.getWord().equals(GameStage.currentWord)){ // if already equals
			              currentBubble.setBubbleDead(); // shark will die
			              this.bubblePop = true;
			              this.timer = 0;
			              GameStage.currentWord = "";
			              this.valid = false;
			              play_bubble_audio();
			          }
		        }
			}
		}



	}

	void removeRenderTopPanel(){
		for(Text tx: topPanel.texts){
			topPanel.root2.getChildren().remove(tx);
		}
	}

	public void play_death_audio(){

		//AudioClip audio = new AudioClip(this.getClass().getResource("click4.wav").toString());
		//audio.play();
		Media media = new Media(new File("src/DooDoo/footstep_grass_002.wav").toURI().toString());
		MediaPlayer mediaPlayer = new MediaPlayer(media);
		mediaPlayer.play();
	}

	public void play_zapper_audio(){

		//AudioClip audio = new AudioClip(this.getClass().getResource("click4.wav").toString());
		//audio.play();
		Media media = new Media(new File("src/DooDoo/laser8.wav").toURI().toString());
		MediaPlayer mediaPlayer = new MediaPlayer(media);
		mediaPlayer.play();
	}
	
	public void play_bubble_audio(){

		//AudioClip audio = new AudioClip(this.getClass().getResource("click4.wav").toString());
		//audio.play();
		Media media = new Media(new File("src/DooDoo/phaserUp7.wav").toURI().toString());
		MediaPlayer mediaPlayer = new MediaPlayer(media);
		mediaPlayer.play();
	}


}

