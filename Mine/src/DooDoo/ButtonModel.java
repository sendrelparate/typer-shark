package DooDoo;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Font;

public class ButtonModel extends Button {

	private final String FONT_PATH = "src/images/kenvector_future.ttf";
	private final String BUTTON_PRESSED_STYLE = "-fx-background0color: transparent; -fx-background-image: url('/images/blue_button_pressed.png');";
	private final String BUTTON_STYLE = "-fx-background0color: transparent; -fx-background-image: url('/images/blue_button.png');";

	public ButtonModel (String text) {


		setText(text);
		setButtonFont();
		setPrefWidth(190);
		setPrefHeight(49);
		setStyle(BUTTON_STYLE);
		buttonListeners();
	}

	private void setButtonFont() {


		try {
			setFont(Font.loadFont(new FileInputStream(FONT_PATH), 19));
		} catch (FileNotFoundException e) {
			setFont(Font.font("Verdana", 19));
		}

	}

	private void setButtonPressedStyle() {


		setStyle(BUTTON_PRESSED_STYLE);
		setPrefHeight(45);
		setLayoutY(getLayoutY() + 4);
	}

	private void setButtonStyle() {


		setStyle(BUTTON_STYLE);
		setPrefHeight(49);
		setLayoutY(getLayoutY() - 4);
	}

	private void buttonListeners() {


		setOnMousePressed(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent e) {
				if(e.getButton().equals(MouseButton.PRIMARY)) {
					setButtonPressedStyle();
				}

			}
		});

		setOnMouseReleased(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent e) {
				if(e.getButton().equals(MouseButton.PRIMARY)) {
					setButtonStyle();
				}

			}
		});


		setOnMouseEntered(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent e) {
				setEffect(new DropShadow());
			}
		});

		setOnMouseExited(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent e) {
				setEffect(null);
			}
		});

	}
}
