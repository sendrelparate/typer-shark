package DooDoo;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;

import javafx.scene.image.Image;

import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.media.AudioClip;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;



public class Menu {
	private static final int WIDTH = 1000;
	private static final int HEIGHT = 800;
	private AnchorPane mainPane;
	private Scene mainScene;
	private Stage mainStage;

	private static final int START_BUTTON_X = 405;
	private static final int INITIAL_BUTTON_Y = 400;
	private int button_y;
	private GameStage gameStage;


	List<ButtonModel> menuButtons;
	private MenuSubScene instructionSubscene;
	private MenuSubScene aboutUsSubscene;

	private MenuSubScene hideScene;





	public Menu() {
		menuButtons = new ArrayList<>();
		mainPane = new AnchorPane();
		mainScene = new Scene(mainPane, WIDTH, HEIGHT);
		mainStage = new Stage();
		mainStage.setScene(mainScene);
		createButtons();
		createBackground();
		createSubScenes();


		button_y = 0;

		//TyperSharkSubScene subScene = new TyperSharkSubScene();

		//mainPane.getChildren().add(subScene);

	}



	public Stage getMainStage() {
		return mainStage;
	}

	private void addButtons(ButtonModel button) {
		button.setLayoutX(START_BUTTON_X);
		button.setLayoutY(INITIAL_BUTTON_Y + button_y);
		this.button_y += 100;
		menuButtons.add(button);
		mainPane.getChildren().add(button);
	}

	private void createButtons() {
		addStartButton();
		addInstructionButton();
		addAboutUs();
		addExitButton();


	}

	private void createSubScenes() {
		instructionSubscene = new MenuSubScene();
		aboutUsSubscene = new MenuSubScene();
		mainPane.getChildren().add(aboutUsSubscene);
		mainPane.getChildren().add(instructionSubscene);
	}

	private void showSubScene(MenuSubScene subScene){

		if(hideScene != null){
			hideScene.moveSubScene();
		}

		if(hideScene != subScene){
			subScene.moveSubScene();
			hideScene = subScene;
		}else {
			hideScene = null;
		}

	}

	private void addStartButton() {
		ButtonModel startButton = this.start();
		addButtons(startButton);
		/*
		startButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				startSubScene.moveSubScene();
			}
		});
		*/

	}

	private ButtonModel start() {
		ButtonModel startButton = new ButtonModel("PLAY");

		startButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				play_audio_button();
				gameStage = new GameStage();
				gameStage.newGame(mainStage);
			}
		});

		return startButton;

	}

	private void addInstructionButton() {
		ButtonModel instructionButton = new ButtonModel("HOW TO PLAY");
		addButtons(instructionButton);

		instructionButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				play_audio_button();
				instructionSubscene.contentInstruction();
				showSubScene(instructionSubscene);

			}
		});

	}

	private void addAboutUs() {
		ButtonModel aboutUsButton = new ButtonModel("ABOUT US");
		addButtons(aboutUsButton);

		aboutUsButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				play_audio_button();
				aboutUsSubscene.contentAboutUs();
				showSubScene(aboutUsSubscene);
			}
		});
	}

	private void addExitButton() {
		ButtonModel exitButton = new ButtonModel("QUIT");
		addButtons(exitButton);
		
		exitButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				play_audio_button();
				System.exit(0);
			}
		});
	}


	private void createBackground() {
		Image backgroundImage = new Image("images/ocean_background.png", true);
		BackgroundImage background = new BackgroundImage(backgroundImage, BackgroundRepeat.REPEAT, BackgroundRepeat.REPEAT, BackgroundPosition.DEFAULT, null);
		mainPane.setBackground(new Background(background));
	}

	public void play_audio_button(){

		//AudioClip audio = new AudioClip(this.getClass().getResource("click4.wav").toString());
		//audio.play();
		Media media = new Media(new File("src/DooDoo/click4.wav").toURI().toString());
		MediaPlayer mediaPlayer = new MediaPlayer(media);
		mediaPlayer.play();
	}



}
