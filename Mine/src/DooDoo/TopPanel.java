package DooDoo;


import java.util.ArrayList;



import javafx.scene.SubScene;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;

import javafx.scene.text.Text;


public class TopPanel extends SubScene{


	AnchorPane root2;
	ArrayList<Text> texts;
	ProgressBar pb;

	public TopPanel() {
		super(new AnchorPane(), 1280, 40);
		prefWidth(1280);
		prefHeight(40);
		texts = new ArrayList<Text>();
		BackgroundImage bg = new BackgroundImage(new Image("/images/ocean_background.png", 1280, 40, false, true), BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, null);

		root2 = (AnchorPane) this.getRoot();

		root2.setBackground(new Background(bg));

		pb = new ProgressBar();
		pb.setLayoutX(950);
		pb.setLayoutY(12);
		root2.getChildren().add(pb);

		setLayoutX(0);
		setLayoutY(0);

	}

	public void setProgressBar(double x){
		this.pb.setProgress(x);
	}


}
