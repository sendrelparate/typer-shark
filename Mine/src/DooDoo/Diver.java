package DooDoo;


import javafx.scene.image.Image;

class Diver extends Sprite{
	public final static int FINAL_YPOS = 300;
	public final static int TOTAL_DEPTH = 100;
	public final static int DIVER_WIDTH = 150;
	public final static int DIVER_HEIGHT = 150;
	private final static Image DIVER_IMAGE = new Image("images/ScDiver.png",Diver.DIVER_WIDTH,Diver.DIVER_HEIGHT,false,false); //check if right

	private float depth;

	private int lives;
	private int points;
	private boolean isZapper;
	private double zapperEnergy;
	private boolean isAlive;

	public Diver(String name, int xPos, int yPos){
		super(xPos, yPos);

		this.depth = 0;
		this.lives = 3;

		this.loadImage(DIVER_IMAGE);
		this.isZapper = false;
		this.zapperEnergy = 0;
		this.isAlive = true;
	}

	public void decLives(){
		this.lives-=1;
	}
	public int getLives(){
		return this.lives;
	}
	public void incPoints(Shark shark) {
		this.points+=shark.getPoints();
		this.points+=shark.getX();
		System.out.println(shark.getX());
	}
	public int getplayerPoints() {
		return this.points;
	}

	public float getDepth(){
		return this.depth;
	}
	public void incDepth() {
		if (this.lives>0) {
			this.depth +=0.02f;
		}
	}

	public void move(){
		if(this.getY()<=Diver.FINAL_YPOS){
			this.incYPos(1);
		}
	}

	public boolean isAtBottom(){
		if (this.depth >=Diver.TOTAL_DEPTH) {
			return true;
		}
		return false;
	}
	public boolean isAlive(){
		if(this.lives<=0) {
		return false;
	}
		return true;
	}

	public boolean isZapperAvailable() {
		return this.isZapper;
	}

	public void incEnergy(){
		if(this.zapperEnergy + 0.0008 > 1 && this.lives>0){
	    	this.zapperEnergy = 1;
		}else if(this.lives>0){
			this.zapperEnergy += 0.0008;
		}
 	}



	public double getEnergy(){
		return this.zapperEnergy;
 	}

	public void setZapperAvailable(){
		if(this.zapperEnergy == 1){
			this.isZapper = true;
		}else {
			this.isZapper = false;
		}
	}

	public void resetEnergy() {
		this.zapperEnergy = 0;
	}




}