package DooDoo;

import javafx.scene.image.Image;


public class Shark extends Sprite{
	protected String word;
	protected String displayWord;
	public final static Image SHARK_IMAGE = new Image("images/shark.gif",Shark.SHARK_WIDTH,Shark.SHARK_HEIGHT,false,false);
	public final static Image EXPLOSION = new Image("images/explosion-big.gif");
	public final static Image SHARK_DEAD = new Image("images/dead_Fish.png");
	public final static Image SHARK_DEAD2 = new Image("images/dead_Fish2.png");
	public final static int SHARK_WIDTH =150;
	public final static int SHARK_HEIGHT = 150;
	private boolean alive;

	private double origSpeed;
	private double speed;
	private int points;
	private boolean shark_explosion;

	public Shark(double x, double y,String word, int points){
		super(x,y);
		this.alive = true;
		this.loadImage(Shark.SHARK_IMAGE);
		this.shark_explosion = false;
		this.word = word;
		this.displayWord = word;
		this.points = points;
		this.speed = 1;
		this.origSpeed = speed;

	}
	public boolean isAlive(){
		return this.alive;
	}
	
	public boolean getSharkExplosion() {
		return this.shark_explosion;
	}
	
	public void setSharkExplosion() {
		this.shark_explosion = true;
	}

	public void setDisplayWord(String word) {
		this.displayWord = word;
	}

	public boolean isDead(){
		this.alive = false;
		return this.alive;
	}

	public void setSpeed(double speed){
		this.speed = speed;
	}

	public double getOrigSpeed(){
		return this.origSpeed;
	}

	public double getSpeed(){
		return this.speed;
	}

	void move(){
		//if(this.move && this.x < (0 + Shark.SHARK_WIDTH)){
			this.moveLeft(speed);
		//}else {

		//}
	}
	public int getPoints() {
		return this.points;
	}

	public void changeImg(Image image) {
		this.loadImage(image);
	}

	public String getWord() {
		return this.word;
	}

	public void setWord(String word) {
		this.word = word;
	}
	
	public String getDisplayWord() {
		return this.displayWord;
	}
}