package DooDoo;

import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
//import javafx.scene.image.Image;

public class GameStage{
	public static final int WINDOW_WIDTH = 1280;
	public static final int WINDOW_HEIGHT = 800;
	//public static final Image BACKGROUND = new Image("images/background1.png");
	private Scene scene;
	private Stage stage;
	private Group root;
	private Canvas canvas;
	private GraphicsContext gc;
	private GameTimer gametimer;
	static String currentWord = "";
	private Stage mainStage;


	public GameStage(){
		this.root = new Group();
		this.scene = new Scene(root,GameStage.WINDOW_WIDTH,GameStage.WINDOW_HEIGHT,Color.CORNFLOWERBLUE);
		this.canvas = new Canvas(GameStage.WINDOW_WIDTH,GameStage.WINDOW_HEIGHT);
		this.gc = canvas.getGraphicsContext2D();
		this.gametimer = new GameTimer(this.gc,this.scene, this.root);
	}

	public void setStage(Stage stage){
		this.stage = stage;
		this.root.getChildren().add(canvas);
		this.stage.setTitle("Daddy Shark Doo Doo Doo Doo");
		this.stage.setScene(this.scene);
		this.gametimer.start();


	}

	public void newGame(Stage stage) {
		this.mainStage = stage;
		this.mainStage.hide();

		setStage(stage);

		this.stage.show();
	}


}