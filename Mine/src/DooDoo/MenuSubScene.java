package DooDoo;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import javafx.animation.TranslateTransition;

import javafx.scene.SubScene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.util.Duration;

public class MenuSubScene extends SubScene{

	private final String FONT_PATH = "src/images/kenvector_future.ttf";

	private boolean isHidden;
	AnchorPane root2;

	public MenuSubScene() {
		super(new AnchorPane(), 600, 350);
		prefWidth(600);
		prefHeight(350);

		BackgroundImage bg = new BackgroundImage(new Image("/images/green_panel.png", 600, 350, false, true), BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, null);

		root2 = (AnchorPane) this.getRoot();

		root2.setBackground(new Background(bg));

		isHidden = true;



		setLayoutX(200);
		setLayoutY(-1000);
		// TODO Auto-generated constructor stub
	}

	public void moveSubScene() {
		TranslateTransition transition = new TranslateTransition();
		transition.setDuration(Duration.seconds(0.5));
		transition.setNode(this);

		if(isHidden) {
			transition.setToY(1000);
			isHidden = false;
		}else {
			transition.setToY(0);
			System.out.println("pumasok");
			isHidden = true;
		}


		transition.play();
	}

	public void contentInstruction() {
		Text word = new Text("# Instructions \n"
				+ "In this game, you will be the diver. You will be faced with sharks \nthat  will try to kill you. In order to play this game well read the \n following: \n" +
				"\t * Place your hands on your keyboard and get ready to type the \n \t words or letters you would see in the sharks or bubbles that \n\t appears. \n" +
				"\t* When the shark touches you, you will lose 1 life. You will only \n\t be given 3 lives and the game will end after losing all of your\n\t lives.\n" +
				"\t* In case of a panic, you can use the �Shark Zapper�. if it is full,\n\t to kill all the sharks. \n\n" +
				"Press �Space Bar� to Pause the game. \n\n" +
				"Notes: \n" +
				"\t* Shark Zapper fills up over time by itself and you will see it in\n\t the progress bar at the bottom of the game.\n" +
				"\t* Shark speeds up as you go deeper.\n" +
				"\t* Accumulated points will be higher when the shark is farther\n\t from you. So you better type fast!");

		try {
			word.setFont(Font.loadFont(new FileInputStream(FONT_PATH), 12));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		word.setLayoutX(25);
		word.setLayoutY(25);
		this.root2.getChildren().add(word);
	}

	public void contentAboutUs() {
		Text word = new Text("# About Us \n\n"+
				"\tThe developers of this game are Edward Julius Dawal and \n Sendrel Van Parate. \n\n" +
				"\tNone of the two are under the BSCS-curriculum. Dawal is under \nBSAP  while"+
				" Parate is an BSABT student.");
		try {
			word.setFont(Font.loadFont(new FileInputStream(FONT_PATH), 12));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		word.setLayoutX(25);
		word.setLayoutY(25);
		this.root2.getChildren().add(word);
	}

}
